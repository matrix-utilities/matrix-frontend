
# Matrix Frontend

This project is a matrix utility frontend bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

### `npm start`
Runs the app in development mode. Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

### `npm test`
Launches the test runner in interactive watch mode.

### `npm run build`
Builds the app for production to the `build` folder.

### `npm run eject`
Removes the single build dependency from your project.

Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

## Configuration

### .env file

To bypass the dependency tree check, add the following line to a `.env` file in the root of your project:

```
SKIP_PREFLIGHT_CHECK=true
REACT_APP_MATRIX_API_URL=http://127.0.0.1:8080/matrix/api/v1
REACT_APP_LOGIN=http://127.0.0.1:8080
REACT_APP_STATS_API_URL=http://127.0.0.1:8081/stats/api/v1
```

### Build the Docker Image

```sh
docker build -t matrix-frontend .
```

### Run the Docker Container

```sh
docker run -p 3000:3000 matrix-frontend
```

## Despliegue con gcloud (Example):

```sh
gcloud.cmd builds submit --tag us-east4-docker.pkg.dev/project/matrix-utilities/project:dev .
```
## Author
By [Orlando A. Yepes](https://www.linkedin.com/in/orlando-andr%C3%A9s-yepes-miquilena-9649544a/)
