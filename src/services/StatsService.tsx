import axios from 'axios';
import { StatsResponse } from '../interfaces/statsService.interface';


const BASE_URL = process.env.REACT_APP_STATS_API_URL;

export const StatsService = {
    async calculateStats(matrix: number[][], token: string): Promise<StatsResponse> {
        try {
            const response = await axios.post(`${BASE_URL}/calculate`, { data: matrix }, {
                headers: { 'Content-Type': 'application/json', 'Authorization': token }
            });
            return response.data;
        } catch (error: any) {
            throw new Error('Failed to calculate matrix stats: ' + error.message);
        }
    }
};
