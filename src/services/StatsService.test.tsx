import axios from 'axios';
import { StatsService } from './StatsService';

jest.mock('axios');
const mockedAxios = axios as jest.Mocked<typeof axios>;

describe('StatsService', () => {
    it('calculates stats', async () => {
        const matrix = [[1, 2], [3, 4]];
        const stats = { max: 4, min: 1, average: 2.5, totalSum: 10, isDiagonal: false };
        mockedAxios.post.mockResolvedValue({ data: { code: '01', data: stats } });

        const result = await StatsService.calculateStats(matrix, '');
        expect(result.data).toEqual(stats);
    });
});
