import axios from 'axios';
import { MatrixService } from './MatrixService';

jest.mock('axios');
const mockedAxios = axios as jest.Mocked<typeof axios>;

describe('MatrixService', () => {
    it('rotates matrix', async () => {
        const matrix = [[1, 2], [3, 4]];
        const rotatedMatrix = [[3, 1], [4, 2]];
        mockedAxios.post.mockResolvedValue({ data: { code: '01', data: rotatedMatrix } });

        const result = await MatrixService.rotateMatrix(matrix, '');
        expect(result.data).toEqual(rotatedMatrix);
    });

    it('calculates QR', async () => {
        const matrix = [[1, 2], [3, 4]];
        const qrResult = { Q: [[1, 0], [0, 1]], R: [[1, 2], [0, 1]] };
        mockedAxios.post.mockResolvedValue({ data: { code: '01', data: qrResult } });

        const result = await MatrixService.calculateQR(matrix, '');
        expect(result.data).toEqual(qrResult);
    });
});
