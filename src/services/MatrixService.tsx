import axios from 'axios';
import {LoginResponse, MatrixQRResponse, MatrixRotateResponse} from "../interfaces/matrixService.interface";

const BASE_URL = process.env.REACT_APP_MATRIX_API_URL;
const URI_LOGIN = process.env.REACT_APP_LOGIN;

export const MatrixService = {
    async login(): Promise<LoginResponse> {
        try {
            const userAgent = navigator.userAgent;
            const language = navigator.language;
            const id = `${userAgent}-${language}`;
            const response = await axios.get(`${URI_LOGIN}/login`, {
                headers: { 'Content-Type': 'application/json' },
                params: { id },
            });
            return response.data;
        } catch (error: any) {
            throw new Error('Failed to rotate matrix: ' + error.message);
        }
    },
    async rotateMatrix(matrix: number[][], token: string): Promise<MatrixRotateResponse> {
        try {
            const response = await axios.post(`${BASE_URL}/rotate`, { array: matrix }, {
                headers: { 'Content-Type': 'application/json', 'Authorization': token }
            });
            return response.data;
        } catch (error: any) {
            throw new Error('Failed to rotate matrix: ' + error.message);
        }
    },
    async calculateQR(matrix: number[][], token: string): Promise<MatrixQRResponse> {
        try {
            const response = await axios.post(`${BASE_URL}/qr`, { array: matrix }, {
                headers: { 'Content-Type': 'application/json', 'Authorization': token }
            });
            return response.data;
        } catch (error: any) {
            throw new Error('Failed to fetch QR data: ' + error.message);
        }
    }
};
