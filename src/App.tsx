import React from 'react';
import MatrixCalculator from './components/matrix/MatrixCalculator';
import './styles/App.css';

const App: React.FC = () => {
  return (
      <div className="app-container">
        <h1 className="text-center text-3xl font-bold my-4">Matrix Operation Calculator</h1>
        <MatrixCalculator />
      </div>
  );
};

export default App;