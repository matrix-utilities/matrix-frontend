import React from 'react';
import { render, fireEvent, screen } from '@testing-library/react';
import ErrorPopup from './ErrorPopup';

describe('ErrorPopup', () => {
    // Test para verificar que el componente muestra el mensaje de error correctamente
    it('displays the error message', () => {
        const testMessage = 'Something went wrong!';
        render(<ErrorPopup message={testMessage} onClose={() => {}} />);

        expect(screen.getByText('Error')).toBeInTheDocument();
        expect(screen.getByText(testMessage)).toBeInTheDocument();
    });

    // Test para verificar que el botón de cierre llama a la función onClose
    it('calls onClose when the close button is clicked', () => {
        const onClose = jest.fn();
        render(<ErrorPopup message="Error occurred" onClose={onClose} />);

        const closeButton = screen.getByText('Close');
        fireEvent.click(closeButton);

        expect(onClose).toHaveBeenCalledTimes(1);
    });
});
