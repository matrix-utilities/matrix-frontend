import React from 'react';
import styles from '../../styles/common/ErrorPopup.module.css';

interface ErrorPopupProps {
    message: string;
    onClose: () => void;
}

const ErrorPopup: React.FC<ErrorPopupProps> = ({ message, onClose }) => {
    return (
        <div className={styles.overlay}>
            <div className={styles.popup}>
                <h2>Error</h2>
                <p>{message}</p>
                <button onClick={onClose} className={styles.closeButton}>Close</button>
            </div>
        </div>
    );
};

export default ErrorPopup;
