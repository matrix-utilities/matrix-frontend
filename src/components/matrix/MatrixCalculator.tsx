import React, { useState } from 'react';
import MatrixInput from './MatrixInput';
import ResultsComponent from './ResultsComponent';
import ErrorPopup from '../common/ErrorPopup';
import { useMatrixApi } from '../../hooks/useMatrixApi';
import styles from '../../styles/matrix/MatrixCalculator.module.css';

const MatrixCalculator: React.FC = () => {
    const [matrix, setMatrix] = useState<number[][]>([]);
    const { data, error, isLoading, fetchData } = useMatrixApi();

    const handleSubmit = () => {
        fetchData(matrix);
    };

    return (
        <div className={styles.container}>
            <MatrixInput matrix={matrix} setMatrix={setMatrix} />
            <button onClick={handleSubmit} className={styles.submitButton}>
                Calculate
            </button>
            {isLoading ? <p>Loading...</p> : null}
            {error ? <ErrorPopup message={error} onClose={() => {}} /> : null}
            {data && (
                <>
                    <ResultsComponent rotationMatrix={data.rotationResult} qrMatrix={data.qrResult} stats={data.stats}/>
                </>
            )}
        </div>
    );
};

export default MatrixCalculator;
