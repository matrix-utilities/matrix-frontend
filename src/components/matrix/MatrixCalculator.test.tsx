import React from 'react';
import { render, screen, fireEvent } from '@testing-library/react';
import { act } from 'react';
import '@testing-library/jest-dom/extend-expect';
import MatrixCalculator from './MatrixCalculator';
import { useMatrixApi } from '../../hooks/useMatrixApi';
import { MatrixApiHook, DataMatrixApi } from '../../interfaces/matrixHook.interface';

// Mock the hook
jest.mock('../../hooks/useMatrixApi');

const mockUseMatrixApi = useMatrixApi as jest.MockedFunction<() => MatrixApiHook>;

const createMockDataMatrixApi = (): DataMatrixApi => ({
    rotationResult: [[1, 2], [3, 4]],
    qrResult: {
        Q: [[1, 0], [0, 1]],
        R: [[1, 2], [0, 1]],
        message: '',
        example: null
    },
    stats: {
        rotation: {
            max: 4,
            min: 1,
            average: 2.5,
            totalSum: 10,
            isDiagonal: false
        },
        q: {
            max: 1,
            min: 0,
            average: 0.5,
            totalSum: 2,
            isDiagonal: true
        },
        r: {
            max: 2,
            min: 0,
            average: 1,
            totalSum: 4,
            isDiagonal: false
        }
    }
});

describe('MatrixCalculator', () => {
    beforeEach(() => {
        jest.clearAllMocks();
    });

    it('renders the MatrixInput component', async () => {
        mockUseMatrixApi.mockReturnValue({
            data: createMockDataMatrixApi(),
            error: null,
            isLoading: false,
            fetchData: jest.fn()
        });

        await act(async () => {
            render(<MatrixCalculator />);
        });

        expect(screen.getByLabelText(/Matrix Size/i)).toBeInTheDocument();
    });

    it('calls fetchData when the Calculate button is clicked', async () => {
        const fetchData = jest.fn();
        mockUseMatrixApi.mockReturnValue({
            data: createMockDataMatrixApi(),
            error: null,
            isLoading: false,
            fetchData
        });

        await act(async () => {
            render(<MatrixCalculator />);
        });

        const button = screen.getByText(/Calculate/i);
        fireEvent.click(button);

        expect(fetchData).toHaveBeenCalled();
    });

    it('displays loading message when isLoading is true', async () => {
        mockUseMatrixApi.mockReturnValue({
            data: createMockDataMatrixApi(),
            error: null,
            isLoading: true,
            fetchData: jest.fn()
        });

        await act(async () => {
            render(<MatrixCalculator />);
        });

        expect(screen.getByText(/loading/i)).toBeInTheDocument();
    });

    it('displays an error message when there is an error', async () => {
        mockUseMatrixApi.mockReturnValue({
            data: createMockDataMatrixApi(),
            error: 'Something went wrong',
            isLoading: false,
            fetchData: jest.fn()
        });

        await act(async () => {
            render(<MatrixCalculator />);
        });

        expect(screen.getByText(/something went wrong/i)).toBeInTheDocument();
    });

    it('displays ResultsComponent when data is available', async () => {
        const data = createMockDataMatrixApi();

        mockUseMatrixApi.mockReturnValue({
            data,
            error: null,
            isLoading: false,
            fetchData: jest.fn()
        });

        await act(async () => {
            render(<MatrixCalculator />);
        });

        expect(screen.getByText(/Rotated Matrix/i)).toBeInTheDocument();
        expect(screen.getByText(/QR Result: Q/i)).toBeInTheDocument();
        expect(screen.getByText(/QR Result: R/i)).toBeInTheDocument();
    });
});
