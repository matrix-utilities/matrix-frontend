import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import MatrixDisplay from './MatrixDisplay';

describe('MatrixDisplay', () => {
    it('renders the matrix correctly', () => {
        const matrix = [
            [1, 2, 3],
            [4, 5, 6],
            [7, 8, 9]
        ];

        render(<MatrixDisplay matrix={matrix} />);

        matrix.forEach(row => {
            row.forEach(value => {
                const elements = screen.queryAllByText(value.toString());
                expect(elements.length).toBeGreaterThan(0);
            });
        });
    });

    it('renders a single element matrix correctly', () => {
        const matrix = [[42]];

        render(<MatrixDisplay matrix={matrix} />);

        const elements = screen.queryAllByText('42');
        expect(elements.length).toBeGreaterThan(0);
    });
});
