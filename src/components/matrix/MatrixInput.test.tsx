import React from 'react';
import { render, screen, fireEvent } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import MatrixInput from './MatrixInput';

describe('MatrixInput', () => {
    const setup = (initialMatrix: number[][]) => {
        const setMatrix = jest.fn();
        render(<MatrixInput matrix={initialMatrix} setMatrix={setMatrix} />);
        return { setMatrix };
    };

    it('renders the matrix size input', () => {
        setup([[0, 0, 0], [0, 0, 0], [0, 0, 0]]);
        expect(screen.getByLabelText(/matrix size/i)).toBeInTheDocument();
    });

    it('renders the correct initial matrix', () => {
        setup([[0, 1, 2], [4, 5, 6], [7, 8, 9]]);
        expect(screen.getByDisplayValue('0')).toBeInTheDocument();
        expect(screen.getByDisplayValue('1')).toBeInTheDocument();
        expect(screen.getByDisplayValue('2')).toBeInTheDocument();
        expect(screen.getByDisplayValue('4')).toBeInTheDocument();
        expect(screen.getByDisplayValue('5')).toBeInTheDocument();
        expect(screen.getByDisplayValue('6')).toBeInTheDocument();
        expect(screen.getByDisplayValue('7')).toBeInTheDocument();
        expect(screen.getByDisplayValue('8')).toBeInTheDocument();
        expect(screen.getByDisplayValue('9')).toBeInTheDocument();
    });

    it('calls setMatrix with new size when matrix size is changed', () => {
        const { setMatrix } = setup([[0, 0, 0], [0, 0, 0], [0, 0, 0]]);
        const sizeInput = screen.getByLabelText(/matrix size/i);
        fireEvent.change(sizeInput, { target: { value: '4' } });

        expect(setMatrix).toHaveBeenCalled();
        expect(setMatrix.mock.calls[0][0]).toHaveLength(4);
    });

    it('calls setMatrix with updated value when matrix cell is changed', () => {
        const { setMatrix } = setup([[0, 0, 0], [0, 0, 0], [0, 0, 0]]);
        const cellInput = screen.getAllByDisplayValue('0')[0];
        fireEvent.change(cellInput, { target: { value: '5' } });

        expect(setMatrix).toHaveBeenCalled();
        expect(setMatrix.mock.calls[0][0][0][0]).toBe(5);
    });
});
