import React from 'react';
import katex from 'katex';
import 'katex/dist/katex.min.css';
import '../../styles/matrix/ResultDisplay.module.css';

interface MatrixDisplayProps {
    matrix: number[][];
}

const MatrixDisplay: React.FC<MatrixDisplayProps> = ({ matrix }) => {
    const matrixToTex = (matrix: number[][]): string => {
        return '\\begin{bmatrix}' + matrix.map(row =>
            row.map(Number).join(' & ')
        ).join(' \\\\ ') + '\\end{bmatrix}';
    };

    return (
        <div dangerouslySetInnerHTML={{ __html: katex.renderToString(matrixToTex(matrix), { throwOnError: false }) }} />
    );
};

export default MatrixDisplay;