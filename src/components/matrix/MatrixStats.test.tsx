import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import MatrixStats from './MatrixStats';

describe('MatrixStats', () => {
    const stats = {
        max: 10,
        min: 1,
        average: 5.5,
        totalSum: 55,
        isDiagonal: false,
    };

    it('renders the MatrixStats component with correct statistics', () => {
        render(<MatrixStats stats={stats} />);

        expect(screen.getByText(/matrix statistics/i)).toBeInTheDocument();
        expect(screen.getByText(/maximum:/i)).toHaveTextContent(`Maximum: ${stats.max}`);
        expect(screen.getByText(/minimum:/i)).toHaveTextContent(`Minimum: ${stats.min}`);
        expect(screen.getByText(/average:/i)).toHaveTextContent(`Average: ${stats.average.toFixed(2)}`);
        expect(screen.getByText(/total sum:/i)).toHaveTextContent(`Total Sum: ${stats.totalSum}`);
        expect(screen.getByText(/is diagonal:/i)).toBeInTheDocument();
    });

    it('displays check icon when isDiagonal is true', () => {
        const diagonalStats = { ...stats, isDiagonal: true };
        render(<MatrixStats stats={diagonalStats} />);

        expect(screen.getByText(/is diagonal:/i)).toContainHTML('&#10003;');
    });

    it('displays cross icon when isDiagonal is false', () => {
        render(<MatrixStats stats={stats} />);

        expect(screen.getByText(/is diagonal:/i)).toContainHTML('&#10007;');
    });
});
