import React from 'react';
import styles from '../../styles/matrix/MatrixInput.module.css';

interface MatrixInputProps {
    matrix: number[][];
    setMatrix: (matrix: number[][]) => void;
}

const MatrixInput: React.FC<MatrixInputProps> = ({ matrix, setMatrix }) => {
    const [size, setSize] = React.useState(matrix.length);

    const handleSizeChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        const newSize = parseInt(event.target.value, 10);
        setSize(newSize);
        const newMatrix = Array(newSize).fill(0).map(() => Array(newSize).fill(0));
        setMatrix(newMatrix);
    };

    const handleCellChange = (rowIndex: number, colIndex: number) => (event: React.ChangeEvent<HTMLInputElement>) => {
        const newMatrix = matrix.map((row, rIdx) =>
            row.map((cell, cIdx) => (rIdx === rowIndex && cIdx === colIndex ? parseInt(event.target.value, 10) : cell))
        );
        setMatrix(newMatrix);
    };

    return (
        <div className={styles.container}>
            <div className={styles.inputGroup}>
                <label htmlFor="matrix-size" className={styles.label}>Matrix Size:</label>
                <input
                    id="matrix-size"
                    type="number"
                    value={size}
                    onChange={handleSizeChange}
                    min="2"
                    max="8"
                    className={styles.input}
                />
            </div>
            <div className={styles.matrix}>
                {matrix.map((row, rowIndex) => (
                    <div key={rowIndex} className={styles.row}>
                        {row.map((cell, colIndex) => (
                            <input
                                key={colIndex}
                                type="number"
                                value={cell}
                                onChange={handleCellChange(rowIndex, colIndex)}
                                className={styles.cell}
                            />
                        ))}
                    </div>
                ))}
            </div>
        </div>
    );
};

export default MatrixInput;
