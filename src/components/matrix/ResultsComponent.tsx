import React from 'react';
import MatrixDisplay from './MatrixDisplay';
import styles from '../../styles/matrix/ResultsComponent.module.css';
import {ResultsProps} from "../../interfaces/resultComponent.interface";


const ResultsComponent: React.FC<ResultsProps> = ({ qrMatrix, rotationMatrix, stats }) => {
    return (
        <div className={styles.resultsContainer}>
            <div className={styles.resultBlock}>
                <h2>Rotated Matrix</h2>
                <MatrixDisplay matrix={rotationMatrix} />
                <div className={styles.stats}>
                    <p>Max: {stats.rotation?.max}</p>
                    <p>Min: {stats.rotation?.min}</p>
                    <p>Average: {(stats.rotation?.average || 0).toFixed(2)}</p>
                    <p>Total Sum: {stats.rotation?.totalSum}</p>
                    <p>Is Diagonal: {stats.rotation?.isDiagonal ? 'Yes' : 'No'}</p>
                </div>
            </div>
            <div className={styles.resultBlock}>
                <h2>QR Result: Q</h2>
                <MatrixDisplay matrix={qrMatrix.Q} />
                <div className={styles.stats}>
                    <p>Max: {stats.q?.max}</p>
                    <p>Min: {stats.q?.min}</p>
                    <p>Average: {(stats.q?.average || 0).toFixed(2)}</p>
                    <p>Total Sum: {stats.q?.totalSum}</p>
                    <p>Is Diagonal: {stats.q?.isDiagonal ? 'Yes' : 'No'}</p>
                </div>
            </div>
            <div className={styles.resultBlock}>
                <h2>QR Result: R</h2>
                <MatrixDisplay matrix={qrMatrix.R} />
                <div className={styles.stats}>
                    <p>Max: {stats.r?.max}</p>
                    <p>Min: {stats.r?.min}</p>
                    <p>Average: {(stats.r?.average || 0).toFixed(2)}</p>
                    <p>Total Sum: {stats.r?.totalSum}</p>
                    <p>Is Diagonal: {stats.r?.isDiagonal ? 'Yes' : 'No'}</p>
                </div>
            </div>
        </div>
    );
};

export default ResultsComponent;