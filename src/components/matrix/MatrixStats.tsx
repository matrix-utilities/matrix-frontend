import React from 'react';
import styles from '../../styles/matrix/MatrixStats.module.css';

interface MatrixStatsProps {
    stats: {
        max: number;
        min: number;
        average: number;
        totalSum: number;
        isDiagonal: boolean;
    };
}

const MatrixStats: React.FC<MatrixStatsProps> = ({ stats }) => {
    return (
        <div className={styles.stats}>
            <h3 className={styles.title}>Matrix Statistics</h3>
            <p>Maximum: {stats.max}</p>
            <p>Minimum: {stats.min}</p>
            <p>Average: {stats.average.toFixed(2)}</p>
            <p>Total Sum: {stats.totalSum}</p>
            <p>Is Diagonal: {stats.isDiagonal ? (
                <span className={styles.iconCheck}>&#10003;</span>
            ) : (
                <span className={styles.iconCross}>&#10007;</span>
            )}</p>
        </div>
    );
};

export default MatrixStats;
