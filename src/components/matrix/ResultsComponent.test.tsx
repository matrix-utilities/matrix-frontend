import React from 'react';
import { render, screen, within } from '@testing-library/react';
import { act } from 'react';
import '@testing-library/jest-dom/extend-expect';
import ResultsComponent from './ResultsComponent';
import { ResultsProps } from '../../interfaces/resultComponent.interface';

describe('ResultsComponent', () => {
    const mockStats = {
        rotation: {
            max: 10,
            min: 1,
            average: 5.5,
            totalSum: 55,
            isDiagonal: false
        },
        q: {
            max: 1,
            min: 0,
            average: 0.5,
            totalSum: 2,
            isDiagonal: true
        },
        r: {
            max: 2,
            min: 0,
            average: 1,
            totalSum: 4,
            isDiagonal: false
        }
    };

    const mockQRMatrix = {
        Q: [[1, 0], [0, 1]],
        R: [[1, 2], [0, 1]]
    };

    const mockRotationMatrix = [
        [1, 2],
        [3, 4]
    ];

    const setup = (props: ResultsProps) => {
        act(() => {
            render(<ResultsComponent {...props} />);
        });
    };

    it('renders the rotated matrix and its stats', () => {
        setup({ qrMatrix: mockQRMatrix, rotationMatrix: mockRotationMatrix, stats: mockStats });

        const rotatedMatrixContainer = screen.getByText(/rotated matrix/i).closest('div');

        if (rotatedMatrixContainer) {
            const withinRotatedMatrix = within(rotatedMatrixContainer);
            expect(withinRotatedMatrix.getByText(/max: 10/i)).toBeInTheDocument();
            expect(withinRotatedMatrix.getByText(/min: 1/i)).toBeInTheDocument();
            expect(withinRotatedMatrix.getByText(/average: 5.50/i)).toBeInTheDocument();
            expect(withinRotatedMatrix.getByText(/total sum: 55/i)).toBeInTheDocument();
            expect(withinRotatedMatrix.getByText(/is diagonal: no/i)).toBeInTheDocument();
        }
    });

    it('renders the Q matrix and its stats', () => {
        setup({ qrMatrix: mockQRMatrix, rotationMatrix: mockRotationMatrix, stats: mockStats });

        const qMatrixContainer = screen.getByText(/qr result: q/i).closest('div');

        if (qMatrixContainer) {
            const withinQMatrix = within(qMatrixContainer);
            expect(withinQMatrix.getByText(/max: 1/i)).toBeInTheDocument();
            expect(withinQMatrix.getByText(/min: 0/i)).toBeInTheDocument();
            expect(withinQMatrix.getByText(/average: 0.50/i)).toBeInTheDocument();
            expect(withinQMatrix.getByText(/total sum: 2/i)).toBeInTheDocument();
            expect(withinQMatrix.getByText(/is diagonal: yes/i)).toBeInTheDocument();
        }
    });

    it('renders the R matrix and its stats', () => {
        setup({ qrMatrix: mockQRMatrix, rotationMatrix: mockRotationMatrix, stats: mockStats });

        const rMatrixContainer = screen.getByText(/qr result: r/i).closest('div');

        if (rMatrixContainer) {
            const withinRMatrix = within(rMatrixContainer);
            expect(withinRMatrix.getByText(/max: 2/i)).toBeInTheDocument();
            expect(withinRMatrix.getByText(/min: 0/i)).toBeInTheDocument();
            expect(withinRMatrix.getByText(/average: 1.00/i)).toBeInTheDocument();
            expect(withinRMatrix.getByText(/total sum: 4/i)).toBeInTheDocument();
            expect(withinRMatrix.getByText(/is diagonal: no/i)).toBeInTheDocument();
        }
    });
});
