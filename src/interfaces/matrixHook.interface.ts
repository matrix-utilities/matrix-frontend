import { StatsData } from "./statsService.interface";
import { QRData } from "./matrixService.interface";

export interface DataMatrixApi {
    rotationResult: number[][];
    qrResult: QRData;
    stats: {
        rotation: StatsData;
        q: StatsData;
        r: StatsData;
    },
}

export interface MatrixApiHook {
    data: DataMatrixApi;
    error: string | null;
    isLoading: boolean;
    fetchData: (matrix: number[][]) => void;
}