
export interface MatrixStats {
    max: number;
    min: number;
    average: number;
    totalSum: number;
    isDiagonal: boolean;
}

export interface ResultsProps {
    qrMatrix: {
        Q: number[][];
        R: number[][];
    };
    rotationMatrix: number[][];
    stats: {
        rotation: MatrixStats;
        q: MatrixStats;
        r: MatrixStats;
    };
}