export interface QRData {
    Q: number[][];
    R: number[][];
    message: string | null;
    example: number[][] | null;
}

export interface MatrixData {
    array: number[][];
    message: string | null;
    example: number[][] | null;
}

export interface MatrixRotateResponse {
    code: string;
    data: MatrixData;
    error: string | null;
}

export interface LoginResponse {
    code: string;
    data: {
        token: string;
    };
    error: string | null;
}

export interface MatrixQRResponse {
    code: string;
    data: QRData;
    error: string | null;
}