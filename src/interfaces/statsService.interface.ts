export interface StatsData {
    max: number;
    min: number;
    average: number;
    totalSum: number;
    isDiagonal: boolean;
}

export interface StatsResponse {
    code: string;
    data: StatsData;
    error: string | null;
}