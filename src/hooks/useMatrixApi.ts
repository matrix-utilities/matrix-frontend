import { useState } from 'react';
import { MatrixService } from '../services/MatrixService';
import { StatsService } from '../services/StatsService';
import { MatrixApiHook } from '../interfaces/matrixHook.interface'
import {LoginResponse, MatrixRotateResponse} from "../interfaces/matrixService.interface";

export const useMatrixApi = (): MatrixApiHook => {
    const [data, setData] = useState<any>(null);
    const [error, setError] = useState<string | null>(null);
    const [isLoading, setIsLoading] = useState<boolean>(false);

    const fetchData = async (matrix: number[][]) => {
        setIsLoading(true);
        setError(null);
        try {
            const stats = { rotate: {}, q: {}, r: {}};
            let token = '';
            const loginResponse: LoginResponse = await MatrixService.login();
            if (loginResponse?.code !== '01') {
                setError('Error al autenticar');
                return
            }
            token = loginResponse.data.token;

            const rotationResponse: MatrixRotateResponse = await MatrixService.rotateMatrix(matrix, token);
            if (rotationResponse?.code !== '01') {
                setError(rotationResponse.data.message ? rotationResponse.data.message : 'Error al rotar la matriz');
                return
            }

            const statsResponseRotate = await StatsService.calculateStats(rotationResponse.data.array, token);
            if (statsResponseRotate.code !== '01') {
                setData({
                    rotationResult: rotationResponse.data.array,
                });
                setError('Error al calcular estadísticas de la matriz Rotada');
                return
            }

            const qrResponse = await MatrixService.calculateQR(rotationResponse.data.array, token);
            if (qrResponse.code !== '01') {
                setError(qrResponse.data.message ? qrResponse.data.message : 'Error al factorizar la matriz');
                setData({
                    rotationResult: rotationResponse.data.array,
                    stats
                });
                return
            }

            const statsResponseQ = await StatsService.calculateStats(qrResponse.data.Q, token);
            if (statsResponseQ.code !== '01') {
                setData({
                    rotationResult: rotationResponse.data.array,
                    qrResult: qrResponse.data,
                    stats,
                });
                setError('Error al calcular estadísticas de la matriz Q');
                return
            }
            stats.q = statsResponseQ.data

            const statsResponseR = await StatsService.calculateStats(qrResponse.data.R, token);
            if (statsResponseR.code !== '01') {
                setData({
                    rotationResult: rotationResponse.data.array,
                    qrResult: qrResponse.data,
                    stats,
                });
                setError('Error al calcular estadísticas de la matriz R');
                return
            }
            stats.r = statsResponseR.data
            setData({
                rotationResult: rotationResponse.data.array,
                qrResult: qrResponse.data,
                stats
            });
        } catch (err: any) {
            setError(err.response?.data?.message || err.message || 'Internal error');
        } finally {
            setIsLoading(false);
        }
    };

    return { data, error, isLoading, fetchData };
};
