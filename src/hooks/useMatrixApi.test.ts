import { renderHook, act } from '@testing-library/react-hooks';
import { useMatrixApi } from './useMatrixApi';
import { MatrixService } from '../services/MatrixService';
import { StatsService } from '../services/StatsService';

// Mock services
jest.mock('../services/MatrixService');
jest.mock('../services/StatsService');

describe('useMatrixApi', () => {
    it('fetches data and sets loading state correctly', async () => {
        const matrix = [[1, 2], [3, 4]];
        const rotatedMatrix = [[3, 1], [4, 2]];
        const qrResult = { Q: [[1, 0], [0, 1]], R: [[1, 2], [0, 1]] };
        const stats = { max: 4, min: 1, average: 2.5, totalSum: 10, isDiagonal: false };

        // Mock successful responses
        (MatrixService.rotateMatrix as jest.Mock).mockResolvedValue({ code: '01', data: rotatedMatrix });
        (MatrixService.calculateQR as jest.Mock).mockResolvedValue({ code: '01', data: qrResult });
        (StatsService.calculateStats as jest.Mock).mockResolvedValue({ code: '01', data: stats });

        const { result } = renderHook(() => useMatrixApi());

        await act(async () => {
            result.current.fetchData(matrix);
        });

        expect(result.current.isLoading).toBe(false);
        expect(result.current.data).toEqual({
            rotationResult: rotatedMatrix,
            qrResult,
            stats
        });
        expect(result.current.error).toBeNull();
    });

    it('handles errors correctly', async () => {
        const matrix = [[1, 2], [3, 4]];

        // Mock error response
        (MatrixService.rotateMatrix as jest.Mock).mockRejectedValue(new Error('Rotation Error'));

        const { result } = renderHook(() => useMatrixApi());

        await act(async () => {
            result.current.fetchData(matrix);
        });

        expect(result.current.isLoading).toBe(false);
        expect(result.current.data).toBeNull();
        expect(result.current.error).toBe('Network error');
    });
});
