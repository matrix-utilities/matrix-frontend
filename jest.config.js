module.exports = {
    preset: 'ts-jest',
    testEnvironment: 'jsdom',
    // Define cómo Jest encuentra tus archivos de prueba
    testMatch: [
        "**/src/components/**/*.test.tsx", // Patrón para encontrar tests en componentes
        "**/src/components/**/*.test.ts"   // Si también tienes archivos de test .ts
    ],
    // Configuraciones adicionales que podrías necesitar
    setupFilesAfterEnv: ['<rootDir>/src/setupTests.ts'], // Configura entorno de pruebas, p.ej., para extender expect con jest-dom
    moduleFileExtensions: ['ts', 'tsx', 'js', 'jsx', 'json', 'node'], // Extensiones de archivo que maneja
    transform: {
        '^.+\\.(ts|tsx)$': ['ts-jest', {
            tsconfig: 'tsconfig.jest.json'  // Especifica el archivo tsconfig específico para jest
        }],
    },
    moduleNameMapper: {
        '\\.(css|less|scss|sass)$': 'identity-obj-proxy'  // Mapea los módulos CSS para que Jest pueda manejarlos
    },
    collectCoverage: true, // Si deseas recopilar cobertura de código
    collectCoverageFrom: [ // Define de dónde recoger la cobertura
        "src/**/*.{ts,tsx}",
        "!src/**/*.d.ts",
        "!src/index.tsx",
        "!src/reportWebVitals.ts",
        "!src/setupTests.ts"
    ],
    coverageDirectory: "coverage", // Directorio donde se almacenan los informes de cobertura
    globals: {
        'ts-jest': {
            tsconfig: 'tsconfig.jest.json'
        }
    }
};